# Author: Juan Sebastian Gaona C.

import re
import sys


class _Lexer:
    tokens_data = list()

    def __init__(self):
        self.tokens = list()
        self.comment = False

    def init_tokens_data(self):
        _Lexer.tokens_data.append(("booleano", "booleano", False))
        _Lexer.tokens_data.append(("caracter", "caracter", False))
        _Lexer.tokens_data.append(("entero", "entero", False))
        _Lexer.tokens_data.append(("cadena", "cadena", False))
        _Lexer.tokens_data.append(("real", "real", False))

        _Lexer.tokens_data.append(("funcion_principal", "funcion_principal", False))
        _Lexer.tokens_data.append(("fin_principal", "fin_principal", False))

        _Lexer.tokens_data.append(("funcion_principal", "funcion_principal", False))

        _Lexer.tokens_data.append(("leer", "leer", False))
        _Lexer.tokens_data.append(("imprimir", "imprimir", False))

        _Lexer.tokens_data.append(("si", "si", False))
        _Lexer.tokens_data.append(("entonces", "entonces", False))
        _Lexer.tokens_data.append(("si_no", "si_no", False))
        _Lexer.tokens_data.append(("fin_si", "fin_si", False))

        _Lexer.tokens_data.append(("mientras", "mientras", False))
        _Lexer.tokens_data.append(("hacer", "hacer", False))
        _Lexer.tokens_data.append(("fin_mientras", "fin_mientras", False))

        _Lexer.tokens_data.append(("para", "para", False))
        _Lexer.tokens_data.append(("fin_para", "fin_para", False))

        _Lexer.tokens_data.append(("seleccionar", "seleccionar", False))
        _Lexer.tokens_data.append(("entre", "entre", False))
        _Lexer.tokens_data.append(("caso", "caso", False))
        _Lexer.tokens_data.append(("romper", "romper", False))
        _Lexer.tokens_data.append(("defecto", "defecto", False))
        _Lexer.tokens_data.append(("fin_seleccionar", "fin_seleccionar", False))

        _Lexer.tokens_data.append(("estructura", "estructura", False))
        _Lexer.tokens_data.append(("fin_estructura", "fin_estructura", False))

        _Lexer.tokens_data.append(("funcion", "funcion", False))
        _Lexer.tokens_data.append(("retornar", "retornar", False))
        _Lexer.tokens_data.append(("fin_funcion", "fin_funcion", False))

        _Lexer.tokens_data.append(("[0-9]+", "tk_entero", True))
        _Lexer.tokens_data.append(("[0-9]+\.[0-9]+", "tk_real", True))
        _Lexer.tokens_data.append(("\"[a-zA-Z0-9_\s]*\"", "tk_cadena", True))
        _Lexer.tokens_data.append(("\'[a-zA-Z0-9_\s]*\'", "tk_caracter", True))

        _Lexer.tokens_data.append(("\+", "tk_mas", False))
        _Lexer.tokens_data.append(("-", "tk_menos", False))
        _Lexer.tokens_data.append(("\*", "tk_mult", False))
        _Lexer.tokens_data.append(("/", "tk_div", False))
        _Lexer.tokens_data.append(("%", "tk_mod", False))
        _Lexer.tokens_data.append(("=", "tk_asig", False))
        _Lexer.tokens_data.append(("<", "tk_menor", False))
        _Lexer.tokens_data.append((">", "tk_mayor", False))
        _Lexer.tokens_data.append(("<=", "tk_menor_igual", False))
        _Lexer.tokens_data.append((">=", "tk_mayor_igual", False))
        _Lexer.tokens_data.append(("==", "tk_igual", False))
        _Lexer.tokens_data.append(("&&", "tk_y", False))
        _Lexer.tokens_data.append(("\|\|", "tk_o", False))
        _Lexer.tokens_data.append(("!=", "tk_dif", False))
        _Lexer.tokens_data.append(("!", "tk_neg", False))
        _Lexer.tokens_data.append((":", "tk_dosp", False))
        _Lexer.tokens_data.append(("\'", "tk_comilla_sen", False))
        _Lexer.tokens_data.append(("\"", "tk_comilla_dob", False))
        _Lexer.tokens_data.append((";", "tk_pyc", False))
        _Lexer.tokens_data.append((",", "tk_coma", False))
        _Lexer.tokens_data.append(("\.", "tk_punto", False))
        _Lexer.tokens_data.append(("\(", "tk_par_izq", False))
        _Lexer.tokens_data.append(("\)", "tk_par_der", False))

        _Lexer.tokens_data.append(("[a-zA-Z_]+[a-zA-Z0-9_]*", "id", True))

        _Lexer.tokens_data.append(("[0-9]+\.[0-9\.]+", "error", False))
        _Lexer.tokens_data.append(("[0-9]+[a-zA-Z_\-]+", "error", False))
        _Lexer.tokens_data.append((".", "error", False))

    def add_token(self, token):
        self.tokens.append(token)

    def get_tokens(self):
        return self.tokens

    def scan_lines(self, lines):
        for r, line in enumerate(lines):
            matched_tok = self.match_line(line)

            row = r + 1

            for match in matched_tok:
                tk, col, printable = match

                if tk.token_class == "error":
                    print(">>> Error lexico (linea: {}, posicion: {})".format(row, col))
                    sys.exit()
                else:
                    self.add_token((tk, row, col))
                    if printable:
                        print("<{},{},{},{}>".format(
                            tk.token_class,
                            tk.lexeme,
                            row,
                            col))
                    else:
                        print("<{},{},{}>".format(
                            tk.token_class,
                            row,
                            col))

            self.add_token((_Token("EOF", "EOF"), row, 1))

    def match_line(self, line):
        col = 1
        evaluating = True
        matched_class = str()
        matched_lexeme = str()
        matched_printable = False

        matched = list()

        while evaluating and len(line) > 0:
            if self.comment:
                match = re.match(".*\*/", line)

                if match:
                    line = line.replace(match.group(0), "", 1)
                    col += len(match.group(0))
                    self.comment = False

                    if len(line) == 0:
                        break
                else:
                    break

            match = re.match("\s+", line)

            if match:
                line = line.replace(match.group(0), "", 1)

                if len(line) == 0:
                    break

                col += len(match.group(0))

            match = re.match("//", line)

            if match:
                break

            match = re.match("/\*", line)

            if match:
                self.comment = True
                continue

            for regex, token_class, printable in _Lexer.tokens_data:
                match = re.match(regex, line)

                if match:
                    if len(match.group(0)) > len(matched_lexeme):
                        matched_class = token_class
                        matched_lexeme = match.group(0)
                        matched_printable = printable

            matched.append((_Token(matched_class, matched_lexeme), col, matched_printable))
            line = line.replace(matched_lexeme, "", 1)
            col += len(matched_lexeme)

            matched_class = str()
            matched_lexeme = str()
            matched_printable = False

        return matched


class _Parser:
    def __init__(self):
        self.current_token = 0
        self.tokens = None
        self.parse_words = list()

    def get_next_token(self):
        t = self.tokens[self.current_token]
        self.current_token += 1
        return t

    def do_parse(self, tokens):
        self.tokens = tokens
        self.parse_s()

    def parse_s(self):
        self.parse_words = "DECL funcion_principal STMTS fin_principal DECL EOF".split()
        self.parse_decl()

    def append_front_parse_words(self, string):
        self.parse_words = string.split() + self.parse_words

    def parse_decl(self):
        tk, row, col = self.get_next_token()
        lookahead = tk.lexeme
        self.parse_words.pop(0)

        if lookahead == "funcion":
            self.append_front_parse_words("FUNC DECL")
            self.parse_func()

        elif lookahead == "estructura":
            self.append_front_parse_words("STRCT DECL")
            self.parse_strct()

        elif lookahead == "funcion_principal":
            self.parse_stmts()
        else:
            print("<{}:{}> Error sintactico: se encontro: \"{}\"; se esperaba: \"funcion_principal\", "
                  "\"estructura\", \"funcion\".".format(row, col, lookahead))
            exit(1)

    def parse_func(self):
        self.parse_words.pop(0)
        self.append_front_parse_words("TIPOS id tk_par_izq PARAM tk_par_der hacer STMTS retornar EXPR fin_funcion "
                                      "tk_pyc")

        tk, row, col = self.get_next_token()
        lookahead = tk.lexeme

        if lookahead in ['id', 'caracter', 'caracter', 'booleano', 'real', 'cadena']:
            pass #self.parse_tipos()
        else:
            print("<{}:{}> Error sintactico: se encontro: \"{}\"; se esperaba: \"id\", "
                  "\"caracter\", \"booleano\", \"real\", \"cadena\".".format(row, col, lookahead))
            exit(1)

    def parse_strct(self):
        self.parse_words.pop(0)
        self.append_front_parse_words("id MEMB fin_estructura")

        tk, row, col = self.get_next_token()
        lookahead = tk.lexeme

        if lookahead == 'id':
            self.parse_words.pop(0)
        else:
            print("<{}:{}> Error sintactico: se encontro: \"{}\"; se esperaba: \"id\".".format(row, col, lookahead))
            exit(1)

        tk, row, col = self.get_next_token()
        lookahead = tk.lexeme

        if lookahead in ['id', 'caracter', 'caracter', 'booleano', 'real', 'cadena', 'fin_estructura']:
            pass #TODO: self.parse_memb(self)
        else:
            print("<{}:{}> Error sintactico: se encontro: \"{}\"; se esperaba: \"id\", "
                  "\"caracter\", \"booleano\", \"real\", \"cadena\", \"fin_estructura\".".format(row, col, lookahead))
            exit(1)




class _Token:
    def __init__(self, token_class, lexeme):
        self.token_class = token_class
        self.lexeme = lexeme


class PsiComp:
    def __init__(self):
        self.lines = list()
        self.lex = _Lexer()
        self.pars = _Parser()

    def run(self):
        self.get_lines()
        self.lex.init_tokens_data()
        self.lex.scan_lines(self.lines)
        self.pars.do_parse(self.lex.get_tokens())

    def get_lines(self):
        lines_read = sys.stdin.readlines()
        self.lines = [s.strip("\r\n") for s in lines_read]


if __name__ == "__main__":
    x = PsiComp()
    x.run()
